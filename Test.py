from getDataCatalog import getDataCatalog
import re

msjNotFound = '404-NOT FOUND'
msjIncomplete = 'INCOMPLETE'


# cmd, comando
def isComment(cmd):
    x = re.search("^/\*", str(cmd))
    y = re.search("^\*", str(cmd))
    if x:  # IsComment
        return '/*'
    if y:
        return '*'
    return ''


def divideSentence(lineSAS, endSentence):
    subResultSentence = []
    indiceInitSentence = 0
    sizeSentence = len(lineSAS)
    indiceEndSentence = lineSAS.lower().find(endSentence)

    if indiceEndSentence > indiceInitSentence:
        sizeEndSentence = len(endSentence)
        indiceCompleteSentence: int = indiceEndSentence + sizeEndSentence
        subResultSentence.append(lineSAS[indiceInitSentence:indiceCompleteSentence])
        if indiceCompleteSentence < sizeSentence:
            subResultSentence.append(lineSAS[indiceCompleteSentence:sizeSentence])

    #print(subResultSentence)
    return subResultSentence


def divideSentenceRegex(lineSAS, endSentence):
    subResultSentence = []
    indiceInitSentence = 0
    sizeSentence = len(lineSAS)
    lineSASLower = lineSAS.lower()
    print(endSentence)
    findEndSentence = re.search(endSentence, lineSASLower)

    if findEndSentence:
        indiceEndSentence = findEndSentence.span()

        if indiceEndSentence[0] > indiceInitSentence:
            indiceCompleteSentence: int = indiceEndSentence[1]
            subResultSentence.append(lineSAS[indiceInitSentence:indiceCompleteSentence])
            if indiceCompleteSentence < sizeSentence:
                subResultSentence.append(lineSAS[indiceCompleteSentence:sizeSentence])

    return subResultSentence

def extractSentence(rpta, lineSAS):
    lineSAS = lineSAS.strip()  # clean space
    cmd = lineSAS.split()
    resultDivideSentence = []
    dataCatalog = False

    if cmd:  # Not is empty
        firstWord = cmd[0]
        flagComment = isComment(firstWord)

        if flagComment != '':  # Is coment
            firstWord = flagComment

        dataCatalog = getDataCatalog(firstWord, 'sentence', 'end')
        if dataCatalog:
            for endSentence in dataCatalog:
                resultDivideSentence = divideSentence(lineSAS, endSentence)
                if resultDivideSentence:
                    break  # Termina el for

    if len(resultDivideSentence) == 0:
        rpta.append(lineSAS)
        if dataCatalog:  # Encontro en catalogo?
            rpta.append(msjIncomplete)
        else:
            rpta.append(msjNotFound)
        return rpta

    elif len(resultDivideSentence) == 1:
        rpta.append(resultDivideSentence[0])
        return rpta

    elif len(resultDivideSentence) > 1:
        rpta.append(resultDivideSentence[0])
        return extractSentence(rpta, resultDivideSentence[1])


rpta = []
lineSAS = "Bienvenidos Hola mundo; como estas"
endSentence = "Hola\s\w*;"

findEndSentence = re.search(endSentence, lineSAS)
print(findEndSentence)

#extractSentence(rpta, lineSAS)
#resultDivideSentence = divideSentenceRegex(lineSAS, endSentence)
#print(resultDivideSentence)