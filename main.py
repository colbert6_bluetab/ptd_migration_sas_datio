import logging
from findScriptSAS import findScriptSAS, setNameScript
from extractSentence import extractSentence
from convertSentence import getTypeSentence

#set level logging view 
logging.basicConfig(format='%(levelname)s:%(asctime)s:%(message)s',level=logging.INFO)

# Folder with files sas
pathInput = 'C:/pyexe/PTD/SASaux/'
pathOutput = 'C:/pyexe/PTD/SAS_/'

# Code of id
msjNotFound = '404-NOT FOUND'
msjIncomplete = 'INCOMPLETE'
IdSentencesSASAnalyce = ''

# Variables
sentencesSASAnalyce = {}
sentencesNotFound = []
sentencesType = {"comment": 0}

# Search for sas files in folder
scriptSource = findScriptSAS(pathInput)

for scriptSAS in scriptSource:
    fileInput = open(scriptSAS, 'r')
    nameScriptOuput = setNameScript(scriptSAS)
    fileOutput = open(pathOutput + nameScriptOuput, "w")

    logging.info("--- Inicio de inspección unitaria---")
    logging.info(">> Inspección de : %s", scriptSAS)

    lineSAS = ""
    lineSASCounter = 0
    sentenceSASCounter = 0
    sentenceSASNotFoundCounter = 0

    for line in fileInput:
        lineSASCounter += 1
        if line.strip() != '':  # Omitir espacios en blanco
            lineSAS = lineSAS + line

            rpta = []
            sentenceSAS = extractSentence(rpta, lineSAS)

            # code for incomplete sentences
            if sentenceSAS[-1] == msjIncomplete:
                lineSAS = sentenceSAS[-2] + " "
                sentenceSAS.pop(-1)
                sentenceSAS.pop(-1)

            # code for sentences not found
            elif sentenceSAS[-1] == msjNotFound:
                lineSAS = ""
                sentenceSAS.pop(-1)
                sentencesNotFound.append(sentenceSAS[-1])
                sentenceSAS.pop(-1)
                sentenceSASNotFoundCounter += 1

            else:
                lineSAS = ""

            sentenceSASCounter += len(sentenceSAS)

            # Valid sentence to convert
            for sentence in sentenceSAS:
                dataSentence = getTypeSentence(sentence)

                # logging.info("-> : %s : %s", dataSentence[0], sentence)

                # Set info sentence diccionary
                if dataSentence[0] in sentencesType:
                    sentencesType[dataSentence[0]] += 1
                else:
                    sentencesType[dataSentence[0]] = 1

                if dataSentence[0] in sentencesSASAnalyce:
                    sentencesSASAnalyce[str(dataSentence[0])].append(sentence)
                else:
                    sentencesSASAnalyce[dataSentence[0]] = [sentence]

    logging.info("\t>> Número de lineas :: %s", lineSASCounter)
    logging.info("\t>> Sentencias encontradas :: %s", sentenceSASCounter)
    logging.info("\t>> Sentencias no encontradas = %s", sentenceSASNotFoundCounter)
    logging.info(sentencesType)
    logging.info("--- Fin de inspección unitaria---")

logging.info("--- Datos de analisis global ---")
logging.info(">> Sentencias no encontradas = %s :: ", str(len(sentencesNotFound)))
for sentence in sentencesNotFound:
    logging.info("\t %s",sentence)

logging.info(">> Información Tipos de Sentencias  = %s :: ",str(len(sentencesType)))
logging.info(sentencesType)

#----Mostrar detalle de las sentencias
IdSentencesSASAnalyce = input("Ingrese el tipo a analizar : ")
logging.info(">> Analisis al tipo  = %s :: ",IdSentencesSASAnalyce)
for sentence in sentencesSASAnalyce[IdSentencesSASAnalyce]:
    logging.info(sentence)
logging.info("----Finaliza proceso exitoso----")