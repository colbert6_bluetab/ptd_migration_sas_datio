Libname Env "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Enviados";
Libname Env "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Recibidos";

DATA F507;
INFILE "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Enviados/REGISTROS.TABLA.RVDT507.F200618.TXT" MISSOVER DSD lrecl=32767;

INPUT
@1 APLICATIVO COMMA2.
@3 BANCO COMMA4.
@7 SUC_OBL_AL COMMA4.
@11 DIGITO COMMA2.
@13 NUMERO_OP COMMA10.
@23 FEC_CASTIGO yymmdd10.
@33 TIPO_ID COMMA2.
@35 NIT COMMA15.
@50 DV COMMA1.
@51 FEC_FORMATO yymmdd10.
@61 TP_CAR COMMA1.
@62 Saldo_Capital_Mes_Ant COMMA19.2
@81 Saldo_Interes_Mes_Ant COMMA19.2
@100 Saldo_Otros_Mes_Ant COMMA19.2
@119 Saldo_Capital_Castigo_del_mes COMMA19.2
@138 Saldo_Interes_Castigo_del_mes COMMA19.2
@157 Saldo_Otros_Castigo_del_mes COMMA19.2
@176 Venta_Cartera COMMA19.2
@195 Recup_via_gtia COMMA19.2
@214 Recup_gestion_cobranza COMMA19.2
@233 Condonaciones COMMA19.2
@252 Otros_conceptos_dismin COMMA19.2
@271 Saldo_Capital_Fecha_Corte COMMA19.2
@290 Saldo_Int_Fecha_Corte COMMA19.2
@309 Saldo_Otros_Fecha_Corte COMMA19.2
@328 Diferencia COMMA19.2
@347 DESCRI $41.;
format 	FEC_CASTIGO yymmdd10.
       	FEC_FORMATO yymmdd10.
		DESCRI $41.;
RUN;

data f507mes;
set f507;
where fec_formato = "29may2020"d;
run;


PROC EXPORT DATA = f507mes
            OUTFILE= "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Recibidos/f507mes.xlsx" 
            DBMS=XLSX REPLACE;
     SHEET="f507";
RUN;
