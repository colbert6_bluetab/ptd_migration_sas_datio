
/*Este programa genera archivo para modificación masiva de información de cartera total

1- contrato de  20 posiciones
2- estado 
3- calificacion definitiva
4- edad de mora
5- valor saldo- en miles
6- valor mora - en miles

*/

Libname Env "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Enviados";

dm 'clear log';
options mtrace mprint mlogic COMPRESS=BINARY REUSE=YES;

Data Hora_inicio;
	format 	Hora_inicio time.;
				Hora_inicio = time();
	put 		Hora_inicio = time.;
run;

/*cambiar mes de corte*/
%let mes=abr20;
%let fecha = 0420;
%let corte =0420;

/*se toma de la tabla cambios califica la informacion del corte*/

data masiva_&mes;
set env.tabla_cambios_califica;
keep suc_obl_al
	 numero_op
	 Nueva_califica;
where corte = &corte;
Run;

data masiva_&mes;
set env.tabla_cambios_califica;
keep suc_obl_al
	 numero_op
	 Nueva_califica;
where corte = &corte;
Run;

proc sql;
create table masiva_&mes as
select a.*,
       b.Nit     
	from masiva_&mes a left join env.fm&fecha b on
	   a.suc_obl_al=b.suc_obl_al and	
	   a.numero_op=b.numero_op;
quit;

data masiva_&mes;
set masiva_&mes;
if nit = "."
then delete;
run;
proc sort nodupkey data=masiva_&mes;by suc_obl_al numero_op Nit Nueva_califica ;run;

/*importar archivo cartera total; solo campos requeridos, estructura en e:\trabajo\pdf\Manual-sector-cartera-entidades-TU-v30.pdf:::ESTE ARCHIVO LO COMPARTRE FLOR TORRES*/

data test;
infile "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Enviados/CARTERA_NUEVO_FORMATO -F200430-REPRO-DEF.TXT" missover dsd;
retain Nit tipoid;
input tipo 1 @;
if tipo = "1" then input 
			corte 22-29;
			

if tipo = "2" then input 
									tipoId  2-3 
									Nit  4-18 
									Contrato $ 89-108
									suc_obl_al 93-96
									numero_op 99-108
									califica_ori 116-117
									Edad_mora $ 122-124
									Vr_mora $ 204-215
									Vr_saldo $ 216-227
									Estado $ 120-121
									
							;		
if tipo = "9" then input 
									cant_regis 2-9;
run;


proc sort nodupkey data=test;by tipo suc_obl_al numero_op;run;

proc sql;
create table test as
select a.*,
       b.Nueva_califica     
	from test a left join masiva_&mes b on
	   a.suc_obl_al=b.suc_obl_al and	
	   a.numero_op=b.numero_op;
quit;

data plano;
	set test;
	if Nueva_califica>0 and cal_def ne califica_ori;
	keep Contrato Edad_mora Vr_mora Vr_saldo estado Nueva_califica   ;	
run;

Libname Rec "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Recibidos";


PROC EXPORT DATA = plano
            OUTFILE= "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Recibidos/Cambios_califica_&mes..xlsx" 
            DBMS=XLSX REPLACE;
     SHEET="&mes";
RUN;

data hora_final;
	format 	hora_final time.;
				hora_final = time();
	put 		hora_final = time.;
run;
data tiempo_proceso;
	merge 	hora_inicio hora_final;
run;
data tiempo_proceso;
	set tiempo_proceso;
	format tiempo_proceso time.;
		tiempo_proceso = sum(hora_final,-hora_inicio);
	put hora_inicio = time. hora_final = time. tiempo_proceso time.;
run;
