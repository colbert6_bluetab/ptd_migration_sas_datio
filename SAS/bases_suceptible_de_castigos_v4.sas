
*hola;
dm 'clear log';

/*condiciones para escoger le mejor bases susceptible*

Menor impacto Provisiones Locales y Consolidadas
Mayor días vencidos
Mayor Ratio de Mora (mora=>90)
Mayor Ratio de Mora (mora=>30 y <90)------------xxxxx---------
Mayor Calificación Local y Consolidada
Cliente Incumplido
Cliente Reestruturado
Menor mix de portafolio (Producto-Familia)
Marca de Recuperaciones ultimos 3 meses (
Castigado ultimos 3 años
Castigado actualmente
Castigado Historicamente
Mayor Plazo remanente

Exclusiones: Clientes (BBVA, Fiduciarias); Plataformas no castigo (Factoring;Empleados,Avales,BankTrade)
Clientes con deuda menor a $100,000
Clientes con operaciones de redescuento
Clientes del Estado

*/


options mtrace mprint mlogic COMPRESS=BINARY REUSE=YES;



Data Hora_inicio;
	format 	Hora_inicio time.;
				Hora_inicio = time();
	put 		Hora_inicio = time.;
run;

dm "clear log";
%let corte=0220;
%let maestro=Maestro_0220;/*Maestro_0120_original*/
%let fec_diario=0220; /*mensual el formato es MMYY; diario el formato es _yyyy_mm_dd*/
%let prod_cas=1,12,50,92,96; /*productos que se pueden castigar*/
%let no_castigo=0,8600030201,8002408820,9003360047, 8002260984;
%let monto_min_castigo=100000;
%let monto_x_mora=0.20000001; 
%let max_prov_faltante_local=0.35; /*% de faltante de provisión que se autoriza*/
%let max_prov_faltante_ifrs9=1; /*% de faltante de provisión que se autoriza*/
%let veces_local_ifrs9=1;  /*permite para diferente de vivienda que entre el faltante local y consolidado exista una diferencia de las veces parametrizadas, ejemplo 3.5 veces significa que en consolidado le falte 3.5 la provisión del local*/
%let veces_local_ifrs9_viv=3.5; /*permite unicamente para vivienda que entre el faltante local y consolidado exista una diferencia de las veces parametrizadas, ejemplo 3.5 veces significa que en consolidado le falte 3.5 la provisión del local*/
%let dias=120;
%let no_cartera=0;/*se excluye estas carteras, separar por espacio cada tipo de cartera, ejemplo 0 1 2 3 6  */



proc sql;
	select lin_subpro into: lin_subpro separated by " " from bases.tabla_lineas where linea_sfc contains("LIBRANZA") and linea_sfc not contains("EXLIBRANZA") ;
	select nit into: nit separated by " " from bases.tabla_entes ;
quit;


data cas;
	set bases.ct&corte;
	if lin_subpro in(&lin_subpro) then libranza_cas="S";else libranza_cas="N";
run;

proc summary data=cas ORDER=INTERNAL NWAY MISSING;
output out=t_cas (DROP= _TYPE_)
max(fec_castigo)=fec_cas_max;
class nit libranza_cas;
run;

proc transpose data=t_cas out=cas1 (drop=_NAME_) prefix=libranza_;
	var  fec_cas_max ;
	by nit ;
	id libranza_cas;
run;


/*variables cartera activa*/
data cartera;
	set bases.fm&fec_diario;
		if cta<2000 and prod_alt in(&prod_cas) and nit not in(&no_castigo) and nit not in(&nit);
		keep tp nit suc_obl_al numero_op capital interes cxc prov_capital prov_int prov_cxc plazo_remanente tp_car prod_alt lin_subpro fec_desembolso empleado califica califica_7 cta dias_vencidos dias_vencidos_risk reestructurado diferido;
		if indicador<6 then do capital=saldo_tot;
		                                prov_capital=provis_cap;
									end;
		if indicador>99 then do cxc=saldo_tot;
		                                prov_cxc=provis_cap;
									end;
		interes=sal_intere;
		prov_int=provis_int;

			Plazo_remanente=sum(sum(year(fec_final),-year(fecha_ini_mora))*12, sum(month(FEC_FINAL),-month(fecha_ini_mora)));
			empleado=0;
		if cta=1414 then empleado=1;
		if vr_disp>0 and prod_alt=96 then diferido=vr_disp;
run;


/*totales por contrato*/
proc summary data=cartera ORDER=INTERNAL NWAY MISSING;
output out=cartera1 (DROP= _TYPE_)
sum(capital)=capital
sum(prov_capital)=prov_capital
sum(interes)=interes
sum(prov_int)=prov_int
sum(cxc)=cxc
sum(prov_cxc)=prov_cxc
sum(diferido)=diferido
max(Plazo_remanente)=Plazo_remanente
min(fec_desembolso)=fec_desembolso
max(empleado)=empleado
max(califica)=califica
max(califica_7)=califica_7
max(dias_vencidos_risk)=dias_vencidos_risk
max(reestructurado)=reestructurado;  
class suc_obl_al numero_op tp nit tp_car lin_subpro prod_alt ;
run;  

/*genera por nit los tipos de cartera*/
PROC SORT NODUPKEY DATA=cartera1 OUT=NIT_UNICO;BY TP NIT TP_CAR;RUN;

proc transpose data=nit_unico out=nit_tpcar prefix=tp_car_;
var tp_car;
id tp_car;
by tp nit;
run;

data nit_tpcar;
set nit_tpcar;
	array my_array (*) tp_car_:;
		do i=1 TO DIM (my_array);
			if i = 1 and my_array(i) ne .  then tpcar_nit=compress(my_array(i));
			if i > 1 and my_array(i) ne .  then tpcar_nit=compress(tpcar_nit||my_array(i));	
        end;	
run;
/*asigna tipos de cartera que tiene el cliente*/
proc sql;
	create table cartera1 as
		select a.*,
				b.tpcar_nit
		from cartera1 a  left join   nit_tpcar b on		
				a.tp=b.tp and
				a.nit=b.nit ;		
quit;


/*genera por nit los productos*/

PROC SORT NODUPKEY DATA=cartera1 OUT=NIT_UNICO;BY TP NIT prod_alt;RUN;

proc transpose data=nit_unico out=nit_prod prefix=prod_;
var prod_alt;
id prod_alt;
by tp nit;
run;

data nit_prod;
	set nit_prod;
	array my_array (*) prod_:;
		do i=1 TO DIM (my_array);
			if i = 1 and my_array(i) ne .  then nit_prod=compress(my_array(i)||";");
			if i > 1 and my_array(i) ne .  then nit_prod=compress(nit_prod||my_array(i)||";");	
        end;	
run;



/*asigna tipos de producto que tiene el cliente*/
proc sql;
	create table cartera1 as
		select a.*,
				b.nit_prod
		from cartera1 a  left join   nit_prod b on		
				a.tp=b.tp and
				a.nit=b.nit ;		
quit;


/*cambios calificación*/
data cambios_cal;
	set bases.tabla_cambios_califica;
	where corte=&corte;
run;

proc sort nodupkey data=cambios_cal;by suc_obl_al numero_op;run;

/*asigna cambio de calificación*/
proc sql;
	create table cartera1 as
		select a.*,
				b.nueva_califica,
				b.nueva_califica_7
		from cartera1 a  left join   cambios_cal b on		
				a.suc_obl_al=b.suc_obl_al and
				a.numero_op=b.numero_op ;		
quit;


/*asigna ajuste manual*/
proc sql;
	create table cartera1 as
		select a.*,
				b.provis_cap as ajuste
		from cartera1 a left join  bases.Provmanu_&corte b on
				a.suc_obl_al=b.suc_obl_al and
				a.numero_op=b.numero_op ;		
quit;

/*asigna ajustes de calificación y provisiones del cierre*/
data cartera1;
	set cartera1;		
		if nueva_califica>0 then califica=nueva_califica;
		if nueva_califica_7>0 then califica_7=nueva_califica_7;
		if abs(ajuste)>0 then prov_capital=sum(prov_capital,ajuste) ;
run;


/*Provisiones y Stage IFRS9*/
proc summary data=bases.&maestro ORDER=INTERNAL NWAY MISSING;
output out=Maestro (DROP= _TYPE_)
sum(CMCO_IMP_EAD_ACTUAL)=EAD
sum(CMCO_IMP_PROV_FINAL)=PROV_IFRS9
max(CMCO_IND_STAGE_FINAL )=STAGE;  
class suc_obl_al numero_op CMCO_COD_FAMILIA  TP NIT  TP_CAR;
run;  


/*asigna variables IFRS9*/
proc sql;
	create table cartera1 as
		select a.*,
				b.EAD,
				b.PROV_IFRS9,
				b.STAGE,
				b.CMCO_COD_FAMILIA
		from cartera1 a left join  Maestro b on
				a.suc_obl_al=b.suc_obl_al and
				a.numero_op=b.numero_op ;		
quit;

/*clientes incumplidos*/
data incumplidos;
	set bases.tabla_incumplidos_&corte;
	if tipo_registro_op in(0,1);
	keep suc_obl_al numero_op fec_incum_op;
run;

proc sort data=incumplidos;by suc_obl_al numero_op descending fec_incum_op;run;

proc sort nodupkey data=incumplidos dupout=x ;by suc_obl_al numero_op ;run;

/*asigna incumplidos*/
proc sql;
	create table cartera1 as
		select a.*,
				b.fec_incum_op
		from cartera1 a left join  incumplidos b on
				a.suc_obl_al=b.suc_obl_al and
				a.numero_op=b.numero_op ;		
quit;


/*calculos 1*/

data cartera1;
	set cartera1;
	if dias_vencidos_risk<30 then mora_0_29=sum(capital, interes, cxc);
	if 29<dias_vencidos_risk<91 then mora_30_90=sum(capital, interes, cxc);
	if dias_vencidos_risk>90 then mora_mas_90=sum(capital, interes, cxc);
  
	saldo_activo=sum(capital, interes, cxc);
	saldo_prov_local=sum(prov_capital,prov_int,prov_cxc);

	faltante_prov_local=sum(capital, interes, cxc,-prov_capital,-prov_int,-prov_cxc,-diferido);
	if faltante_prov_local<0 then faltante_prov_local=0;
	faltante_prov_Ifrs9=sum(ead,-prov_ifrs9);
run;


/*totales por cliente segmento  stage y calificación*/
proc summary data=cartera1 ORDER=INTERNAL NWAY MISSING;
output out=base_total (DROP= _TYPE_)
sum(capital)=capital
sum(prov_capital)=prov_capital
sum(interes)=interes
sum(prov_int)=prov_int
sum(cxc)=cxc
sum(prov_cxc)=prov_cxc
sum(saldo_activo)=saldo_activo
sum(saldo_prov_local)=saldo_prov_local
sum(diferido)=diferido
max(Plazo_remanente)=Plazo_remanente
min(fec_desembolso)=fec_desembolso
min(fec_incum_op)=fec_incum_op
max(empleado)=empleado
max(dias_vencidos_risk)=dias_vencidos_risk
max(reestructurado)=reestructurado
sum(mora_0_29)=mora_0_29
sum(mora_30_90)=mora_30_90
sum(mora_mas_90)=mora_mas_90
sum(EAD)=EAD
sum(PROV_IFRS9)=PROV_IFRS9
sum(faltante_prov_local)=faltante_prov_local
sum(faltante_prov_Ifrs9)=faltante_prov_Ifrs9;  
class tp nit tp_car lin_subpro prod_alt CMCO_COD_FAMILIA STAGE califica_7 califica tpcar_nit nit_prod;
run;  


proc sort data= base_total;
	by  faltante_prov_local faltante_prov_Ifrs9 descending dias_vencidos_risk descending stage descending saldo_activo descending ead  
	   descending califica descending califica_7;
run;

/*aplica las condiciones parametricas*/
data base1;
	set base_total;
	if dias_vencidos_risk>=&dias;
	if saldo_activo>&monto_min_castigo or ead>&monto_min_castigo;
	if sum(faltante_prov_Ifrs9/ead)<=&max_prov_faltante_ifrs9 or sum(faltante_prov_local/saldo_activo)<=&max_prov_faltante_local ;
	if sum(mora_mas_90/saldo_activo)>&monto_x_mora;
    if tp_car ne 3 and faltante_prov_local/faltante_prov_Ifrs9<=&veces_local_ifrs9 or tp_car eq 3 and faltante_prov_local/faltante_prov_Ifrs9<=&veces_local_ifrs9_viv ;							      

run;

%Macro cartera;
	%Let k = 1;
	%do %while (%scan(&no_cartera,&k," ") ne );
		%Let cartera = compress(%scan(&no_cartera,&k," "));
		
		data base1;
			set base1;
		if find(tpcar_nit,&cartera)>0 then delete;
		run;

		%let k = %eval(&k+1);
	%end;
%Mend cartera;
%cartera;


/*totales de base que cumple condiciones paramétricas*/

proc summary data=base1 ORDER=INTERNAL NWAY MISSING;
output out=t_base1 (DROP= _TYPE_)
sum(EAD)=EAD
sum(PROV_IFRS9)=PROV_IFRS9
sum(faltante_prov_local)=faltante_prov_local
sum(faltante_prov_Ifrs9)=faltante_prov_Ifrs9
sum(saldo_activo)=saldo_activo
sum(saldo_prov_local)=saldo_prov_local
sum(mora_0_29)=mora_0_29
sum(mora_30_90)=mora_30_90
sum(mora_mas_90)=mora_mas_90;
class CMCO_COD_FAMILIA STAGE califica_7 califica tp_car LIN_SUBPRO;
run;

/*escoje menor impacto en provisiones local vs. provisiones ifrs9 */
data t_base2;
	set t_base1;
	/*if sum(sum(faltante_prov_local,0.01)/ sum(faltante_prov_Ifrs9,0.01))<=1 and tp_car not in(&no_cartera);*/
	suscep=1;
run; 

proc sort nodupkey data=t_base2 out=suscep1;by  CMCO_COD_FAMILIA STAGE califica_7 califica LIN_SUBPRO;run;


proc sql;
	create table suscep1 as
		select a.*,
				b.suscep
		from base1 a left join  suscep1 b on		               
				a.CMCO_COD_FAMILIA=b.CMCO_COD_FAMILIA and
				a.stage=b.stage and
			    a.califica_7=b.califica_7 and
			    a.califica=b.califica and
				a.LIN_SUBPRO=b.LIN_SUBPRO;
quit;

data suscep1;
	set suscep1;
		if mora_0_29=. and mora_30_90 =.;
run;


/*asigna variable susceptible en la base total*/
proc sql;
	create table base_total as
		select a.*,
				b.suscep
		from base_total a left join  suscep1 b on
				a.tp=b.tp and
				a.nit=b.nit and
				a.CMCO_COD_FAMILIA=b.CMCO_COD_FAMILIA and
			    a.califica_7=b.califica_7 and
			    a.califica=b.califica and
				a.LIN_SUBPRO=b.LIN_SUBPRO;		
quit;

/*totaliza los susceptibles por cliente*/
proc summary data=base_total ORDER=INTERNAL NWAY MISSING;
output out=t_suscep1 (DROP= _TYPE_);
	class tp nit suscep;
run;


data t_suscep1;
	set t_suscep1;
	if  suscep=. then suscep=0;
run;

proc sort nodupkey data=t_suscep1;by  tp nit suscep;run;


/*traspone la marca susceptible por cliente para detectar clientes que no son susceptibles porque tienen otros contratos que no cumplen las condiciones de parametria*/
proc transpose data=t_suscep1 out=t_suscep2 prefix=suscep_;
	var _freq_;
	by tp nit;
	id suscep;
run;

/*escoje solo los clientes reales susceptibles de castigar*/
data t_suscep2;
	set t_suscep2;
		if suscep_1 ne . and suscep_0=.;
run;


/*base final de susceptibles para enviar a recuperaciones*/
proc sql;
	create table base_susceptible_recup as
		select a.*,
				b.suscep_1
		from base_total a left join  t_suscep2 b on
				a.tp=b.tp and
				a.nit=b.nit ;		
quit;

data base_susceptible_recup;
	set base_susceptible_recup;
	if suscep_1 ne . and  mora_0_29=. and 	 mora_30_90=.;
run;



/*totales de base que cumple condiciones paramétricas*/

proc summary data=base_susceptible_recup ORDER=INTERNAL NWAY MISSING;
output out=t_susceptible_recup (DROP= _TYPE_)
sum(EAD)=EAD
sum(PROV_IFRS9)=PROV_IFRS9
sum(faltante_prov_local)=faltante_prov_local
sum(faltante_prov_Ifrs9)=faltante_prov_Ifrs9
sum(saldo_activo)=saldo_activo
sum(saldo_prov_local)=saldo_prov_local
sum(mora_0_29)=mora_0_29
sum(mora_30_90)=mora_30_90
sum(mora_mas_90)=mora_mas_90;
class CMCO_COD_FAMILIA STAGE califica_7 califica;
run;

proc sort data= base_susceptible_recup;
	by  faltante_prov_local faltante_prov_Ifrs9 descending dias_vencidos_risk descending stage descending saldo_activo descending ead  
	   descending califica descending califica_7;
run;


/*totales por cliente*/
proc summary data=base_susceptible_recup ORDER=INTERNAL NWAY MISSING;
output out=t_susceptible_recup_cliente (DROP= _TYPE_)
sum(EAD)=EAD_CLIENTE
sum(saldo_activo)=saldo_activo_cliente
sum(faltante_prov_local)=faltante_prov_local_CLIENTE
sum(faltante_prov_Ifrs9)=faltante_prov_Ifrs9_CLIENTE;
class tp nit;
run;

data t_susceptible_recup_cliente;
	set t_susceptible_recup_cliente;
		porcent_max=max(faltante_prov_Ifrs9_CLIENTE/EAD_CLIENTE, faltante_prov_local_CLIENTE/saldo_activo_cliente);
run;



proc sql;
create table base_susceptible_recup as
	select a.*,
			  b.EAD_CLIENTE,
			  b.faltante_prov_local_CLIENTE,
			  b.faltante_prov_Ifrs9_CLIENTE,
			  b.porcent_max
	from base_susceptible_recup a left join t_susceptible_recup_cliente b on
			 a.tp=b.tp and
			 a.nit=b.nit;
quit;

proc sort data=base_susceptible_recup;by porcent_max tp nit descending  EAD_CLIENTE descending faltante_prov_Ifrs9_CLIENTE descending faltante_prov_local_CLIENTE;run;



PROC EXPORT DATA=base_susceptible_recup
            OUTFILE= "E:\base susceptible castigos &corte..xlsx" 
            DBMS=EXCEL REPLACE;
     SHEET="base"; 
     NEWFILE=YES;
RUN;



/****UNA VEZ SE DEFINA LA BASE, SE DEBE COLOCAR LOS CLIENTES EN ESTE ARCHIVO****/

PROC IMPORT OUT=SUSCEPTIBLE_FINAL
            DATAFILE= "E:\consulta_susceptible.xlsx" 
            DBMS=EXCEL REPLACE;
     RANGE="Hoja1$"; 
     GETNAMES=YES;
     MIXED=NO;
     SCANTEXT=YES;
     USEDATE=YES;
     SCANTIME=YES;
RUN;

PROC SORT NODUPKEY DATA=SUSCEPTIBLE_FINAL;BY TP	NIT	;RUN;


proc sql;
	create table SUSCEPTIBLE_FINAL as
		select b.*
		from SUSCEPTIBLE_FINAL a left join  Cartera1 b on
				a.tp=b.tp and
				a.nit=b.nit 		    
				;		
quit;

PROC EXPORT DATA=SUSCEPTIBLE_FINAL
            OUTFILE= "E:\SUSCEPTIBLE_FINAL Xx &corte..xlsx" 
            DBMS=EXCEL REPLACE;
     SHEET="base"; 
     NEWFILE=YES;
RUN;



data hora_final;
	format 	hora_final time.;
				hora_final = time();
	put 		hora_final = time.;
run;
data tiempo_proceso;
	merge 	hora_inicio hora_final;
run;
data tiempo_proceso;
	set tiempo_proceso;
	format tiempo_proceso time.;
		tiempo_proceso = sum(hora_final,-hora_inicio);
	put hora_inicio = time. hora_final = time. tiempo_proceso time.;
run;
