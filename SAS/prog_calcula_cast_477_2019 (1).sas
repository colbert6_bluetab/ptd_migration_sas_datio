
/*LIBNAME F477 BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/f477" ;*/

Libname Env "/SASDataRiesgos/Sas_Archivos/SASDataRiesgos/Enviados";

DM "CLEAR LOG";
/* ENERO 2019 */
proc summary data=env.fm0320 missing nway noprint;
    output out=WORK.SALDOCAST0320 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_MAR;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
/*ME TRAIGO EL ARCHIVO DE ALEX*/
proc summary data=env.castigo0420 missing nway noprint;
    output out=WORK.C_0420 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_ABR;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_abr AS 
SELECT A.*,
  B.prov_mar,
  sum(a.PROV_ABR,-b.prov_mar)as GastoABR
 from WORK.C_0420 A LEFT join WORK.SALDOCAST0320 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

 /* FEBRERO 2019 */
proc summary data=bases.fm0119 missing nway noprint;
    output out=WORK.SALDOCAST0119 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_ENE;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO02 missing nway noprint;
    output out=WORK.C_0219 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_FEB;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_FEB AS 
SELECT A.*,
  B.prov_ene,
  sum(a.PROV_FEB,-b.prov_ENE)as GastoFEB
 from WORK.C_0219 A left join WORK.SALDOCAST0119 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

   /* MARZO 2019 */

proc summary data=bases.FM0219 missing nway noprint;
    output out=WORK.SALDOCAST0219 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_FEB;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO03 missing nway noprint;
    output out=WORK.C_0319 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_MAR;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_MAR AS 
SELECT A.*,
  B.prov_FEB,
  sum(a.PROV_MAR,-b.prov_FEB)as GastoMAR
 from WORK.C_0319 A left join WORK.SALDOCAST0219 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

 /* ABRIL 2019 */
proc summary data= bases.FM0319 missing nway noprint;
    output out=WORK.SALDOCAST0319 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_MAR;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO04 missing nway noprint;
    output out=WORK.C_0419 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_ABR;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_ABR AS 
SELECT A.*,
  B.prov_MAR,
  sum(a.PROV_ABR,-b.prov_MAR)as GastoABR
 from WORK.C_0419 A left join WORK.SALDOCAST0319 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

 /*MAYO 2019*/
proc summary data=bases.fm0419 missing nway noprint;
    output out=WORK.SALDOCAST0419 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_APR;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO05 missing nway noprint;
    output out=WORK.C_0519 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_MAY;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_MAY AS 
SELECT A.*,
  B.prov_APR,
  sum(a.PROV_MAY,-b.prov_APR)as GastoMAY
 from WORK.C_0519 A left join WORK.SALDOCAST0419 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;
 /* JUNIO 2019 */
 
proc summary data=bases.fm0519 missing nway noprint;
    output out=WORK.SALDOCAST0519 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_MAY;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO06 missing nway noprint;
    output out=WORK.C_0619 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_JUN;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_JUN AS 
SELECT A.*,
  B.prov_MAY,
  sum(a.PROV_JUN,-b.prov_MAY)as GastoJUN
 from WORK.C_0619 A left join WORK.SALDOCAST0519 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

   /*JULIO 2019 */
proc summary data=bases.FM0619 missing nway noprint;
    output out=WORK.SALDOCAST0619 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_JUN;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO07 missing nway noprint;
    output out=WORK.C_0719 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_JUL;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_JUL AS 
SELECT A.*,
  B.prov_JUN,
  sum(a.PROV_JUL,-b.prov_JUN)as GastoJUL
 from WORK.C_0719 A left join WORK.SALDOCAST0619 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

   /* AGOSTO 2019 */
proc summary data=bases.FM0719 missing nway noprint;
    output out=WORK.SALDOCAST0719 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_JUL;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO08 missing nway noprint;
    output out=WORK.C_0819 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_AGO;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_AGO AS 
SELECT A.*,
  B.prov_JUL,
  sum(a.PROV_AGO,-b.prov_JUL)as GastoJUL
 from WORK.C_0819 A left join WORK.SALDOCAST0719 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

  /* SEPTIEMBRE 2019 */
proc summary data= bases.FM0819 missing nway noprint;
    output out=WORK.SALDOCAST0819 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_AGO;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO09 missing nway noprint;
    output out=WORK.C_0919 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_SEP;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_SEP AS 
SELECT A.*,
  B.prov_AGO,
  sum(a.PROV_SEP,-b.prov_AGO)as GastoAGO
 from WORK.C_0919 A left join WORK.SALDOCAST0819 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

  /* OCTUBRE 2019 */
proc summary data=bases.FM0919 missing nway noprint;
    output out=WORK.SALDOCAST0919 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_SEP;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO10 missing nway noprint;
    output out=WORK.C_1019 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_OCT;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_OCT AS 
SELECT A.*,
  B.PROV_SEP,
  sum(a.PROV_OCT,-b.prov_SEP)as GastoSEP
 from WORK.C_1019 A left join WORK.SALDOCAST0919 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

  /*NOVIEMBRE 2018 */
proc summary data= bases.fm1119 missing nway noprint;
    output out=WORK.SALDOCAST1019 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_OCT;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=propias.CASTIGO11 missing nway noprint;
    output out=WORK.C_1119 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_NOV;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_NOV AS 
SELECT A.*,
  B.prov_OCT,
  sum(a.PROV_NOV,-b.prov_OCT)as GastoNOV
 from WORK.C_1119 A left join WORK.SALDOCAST1018 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
 quit;

  /*DICIEMBRE 2018 */
proc summary data=bases.FM1219 missing nway noprint;
    output out=WORK.SALDOCAST1119 (DROP= _TYPE_ _FREQ_) 
 sum(PROVIS_CAP)=PROV_NOV;
 WHERE CTA <1500;
   class SUC_OBL_AL NUMERO_OP TP_CAR;
run;
proc summary data=F477.CASTIGO12 missing nway noprint;
    output out=WORK.C_1219 (DROP= _TYPE_ _FREQ_) 
 sum(SALDO_TOT)=PROV_DIC;
   class SUC_OBL_AL NUMERO_OP TP_CAR CALIFICA;
run;
PROC SQL;
CREATE TABLE WORK.CRUCE_DIC AS 
SELECT A.*,
  B.prov_NOV,
  sum(a.PROV_DIC,-b.prov_NOV)as GastoNOV
 from WORK.C_1219 A left join WORK.SALDOCAST1119 B on
 A.Suc_obl_al = B.Suc_obl_al and
 A.Numero_op = B.Numero_op
 where a.numero_op > 0;
quit;
DATA WORK.GASTOACUM;
set WORK.CRUCE_ENE
    WORK.CRUCE_FEB
    WORK.CRUCE_MAR
    WORK.CRUCE_ABR
    WORK.CRUCE_MAY
	WORK.CRUCE_JUN
	WORK.CRUCE_JUL
	WORK.CRUCE_AGO
	WORK.CRUCE_SEP
	WORK.CRUCE_OCT
	WORK.CRUCE_NOV
	WORK.CRUCE_DIC;

 GASTOACUM =SUM(GASTOENE, GASTOFEB,GASTOMAR,GASTOABR, GASTOMAY, GASTOJUN, GASTOJUL, GASTOAGO, GASTOSEP, GASTOOCT  );
 Run;

proc summary data=WORK.GASTOACUM missing nway noprint;
    output out=WORK.resumenACUM (DROP= _TYPE_FREQ_) 
 sum(GastoACUM)=Gasto_fin;
   class TP_CAR CALIFICA;
run;
