from getDataCatalog import getDataCatalog
import re

errorNotFound = '404-NOT FOUND'

# cmd, comando
def isComment(cmd):
    x = re.search("^/\*", str(cmd))
    y = re.search("^\*", str(cmd))
    if x:  # IsComment
        return '/*'
    if y:
        return '*'
    return ''


def divideSentence(lineSAS, endSentence):
    subResultSentence = []
    indiceInitSentence = 0
    sizeSentence = len(lineSAS)
    indiceEndSentence = lineSAS.find(endSentence)


    if indiceEndSentence > indiceInitSentence:
        sizeEndSentence = len(endSentence)
        indiceCompleteSentence: int = indiceEndSentence + sizeEndSentence

        subResultSentence.append(lineSAS[indiceInitSentence:indiceCompleteSentence])
        if indiceCompleteSentence < sizeSentence:
            subResultSentence.append(lineSAS[indiceCompleteSentence:sizeSentence])
    return subResultSentence


def extractSentence(lineSAS):
    lineSAS = lineSAS.strip() #clean space
    cmd = lineSAS.split()
    resultSentence = []

    if cmd:  #Not is empty
        firstWord = cmd[0]
        flagComment = isComment(firstWord)
        if flagComment != '':  # Is coment
            dataCatalog = getDataCatalog('comment_' + str(flagComment), 'sentence', 'end')
            if dataCatalog:
                for endSentence in dataCatalog:
                    resultDivideSentence = divideSentence(lineSAS, endSentence)
        else:
            dataCatalog = getDataCatalog(firstWord, 'sentence', 'end')
            if dataCatalog:
                for endSentence in dataCatalog:
                    resultDivideSentence = divideSentence(lineSAS, endSentence)
            else:
                resultSentence.append(errorNotFound)

        if len(resultDivideSentence) > 1:
            resultSentence.append(resultDivideSentence[0])
            print("Result Sentence")
            print(resultDivideSentence)
            resultSentence.append(extractSentence(resultDivideSentence[1]))
            print("Enviamos ")
            print(resultDivideSentence[1])

    return resultSentence
