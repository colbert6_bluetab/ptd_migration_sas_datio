import os
from findScriptSAS import findScriptSAS, setNameScript
from extractSentence import extractSentence
#from getDataCatalog import getDataCatalog

pathInput = 'C:/pyexe/PTD/SASaux/'
pathOutput = 'C:/pyexe/PTD/SAS_/'
errorNotFound = '404-NOT FOUND'
scriptSource = findScriptSAS(pathInput)
sentences = ""


for scriptSAS in scriptSource:
    fileInput = open(scriptSAS, 'r')
    nameScriptOuput = setNameScript(scriptSAS)
    fileOutput = open(pathOutput + nameScriptOuput, "w")
    print("--- Inicio de inspección ---")
    print(">> Inspección de :", scriptSAS)
    lineSAS = ""
    sentenceNotFoundCounter = 0
    lineSASCounter = 0
    commentSASCounter = 0

    for line in fileInput:
        line = line.strip()
        lineSASCounter += 1
        lineSAS = lineSAS + line

        rpta =[]
        sentenceSAS = extractSentence(rpta,lineSAS)
        print("Result extract sentence")
        print(sentenceSAS)
        '''
        if sentenceSAS:
            if sentenceSAS[0] == errorNotFound:
                lineSAS = ""
                sentenceNotFoundCounter += 1
            elif sentenceSAS[0]:
                for sentenceOutput in sentenceSAS:
                    if( str(sentenceOutput).strip(" ") != "" ):
                        #fileOutput.write('SENTENCIA:')
                        #fileOutput.write("0-"+str(sentenceOutput)+"-0" + os.linesep)
                        #print(sentenceOutput)
                commentSASCounter += 1
                lineSAS = ""
        '''


    print("Número de sentencias no catalogadas  :: ", sentenceNotFoundCounter)
    print("Número de sentencias de comentarios :: ", commentSASCounter)
    print("Número de lineas :: ", lineSASCounter)
    print("--- Fin de inspección ---")