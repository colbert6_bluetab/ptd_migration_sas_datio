import pandas as pd

# Parameters
# key, el identificador de la información a buscar
# sheetName, la pestaña o sección del catalogo
# column, la columna  de la información a retornar

# Constant
excelFile = 'C:/pyexe/PTD/Files/Catalog.xlsx'


def getDataCatalog(key, sheetName, column):
    archivo_excel = pd.read_excel(excelFile, sheetName)
    resultFilter = archivo_excel.loc[archivo_excel['key'] == key.lower()]
    result = resultFilter[column].to_numpy()
    if result:
        return str(result[0]).split("|")
    return result

dataCatalog = getDataCatalog('data', 'sentence', 'end')
print(dataCatalog)