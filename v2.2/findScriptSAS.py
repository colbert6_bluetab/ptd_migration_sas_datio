#re: libreria para expresiones regulares
from os import scandir, getcwd
import os


def findScriptSAS(path= getcwd()):
    return [(arch.path) for arch in scandir(path) if arch.is_file()]

def setNameScript(scriptSAS):
    extScriptOuput = '.scala'
    pathAndFile = os.path.split(scriptSAS)
    fileAndExtension = os.path.splitext(pathAndFile[1])
    return fileAndExtension[0] + extScriptOuput

