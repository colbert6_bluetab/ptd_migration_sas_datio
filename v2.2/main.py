import os
from findScriptSAS import findScriptSAS, setNameScript
from extractSentence import extractSentence
#from getDataCatalog import getDataCatalog

pathInput = 'C:/pyexe/PTD/SASaux/'
pathOutput = 'C:/pyexe/PTD/SAS_/'

msjNotFound = '404-NOT FOUND'
msjIncomplete = 'INCOMPLETE'

scriptSource = findScriptSAS(pathInput)
sentences = ""
sentencesNotFound = []


for scriptSAS in scriptSource:
    fileInput = open(scriptSAS, 'r')
    nameScriptOuput = setNameScript(scriptSAS)
    fileOutput = open(pathOutput + nameScriptOuput, "w")

    print("--- Inicio de inspección ---")
    print(">> Inspección de :", scriptSAS)

    lineSAS = ""
    lineSASCounter = 0

    print("\t>> Sentencia encontradas :: ")

    for line in fileInput:
        lineSASCounter += 1
        if line.strip() != '': #Omitir espacios en blanco
            lineSAS = lineSAS + line

            rpta =[]
            sentenceSAS = extractSentence(rpta, lineSAS)

            #lenResult = len(sentenceSAS)
            if(sentenceSAS[-1] == msjIncomplete):

                lineSAS = sentenceSAS[-2]+" "
                sentenceSAS.pop(-1)
                sentenceSAS.pop(-1)
            elif (sentenceSAS[-1] == msjNotFound):
                #No encontró data en catalogo
                lineSAS = ""
                sentenceSAS.pop(-1)
                sentencesNotFound.append(sentenceSAS[-1])
                sentenceSAS.pop(-1)
            else:
                lineSAS = ""

            #Enviar sentence for convert
            for sentence in sentenceSAS:
                print(sentence)

    print("\t>> Número de lineas :: ", lineSASCounter)
    print("\t>> Sentencias no encontradas = "+ str(len(sentencesNotFound)) + " :: " )
    for sentence in sentencesNotFound:
        print("\t" + sentence)

    print("--- Fin de inspección ---")