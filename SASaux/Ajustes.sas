OPTIONS MPRINT MLOGIC SYMBOLGEN;
%LET Dir = /SASDataRiesgos/FTP_sasus18;
*libname Lib "&Dir.";
%PUT _USER_;

/*****************************************  INICIO  *******************************************/
LIBNAME DATACIR BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/datacir" ;
LIBNAME ARCHIVOS BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/archivos" ;
LIBNAME ARCHIVOS BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/archivos" ;
OPTIONS MPRINT MLOGIC SYMBOLGEN;
%LET Dir = /SASData/FTP_sasus18;
%PUT _USER_;

/*** ASIGNAR AUTO LIBRERIAS - SAS SERVIDOR ***/
%MACRO LIBS(name);
LIBNAME &name.  META LIBRARY=&name.  REPNAME= Foundation  METAOUT=DATA;
%MEND;

Data Hora_inicio;
format Hora_inicio time.;
Hora_inicio = time();
put Hora_inicio = time.;
run;;

%LIBS(G_SG_MIN);
%LIBS(PU_CLIEN);
%LIBS(PU_CMP);
%LIBS(PU_ENDEU);
%LIBS(PU_HERME);  
%LIBS(PU_SEGUI);
%LIBS(SERM_BAS);
%LIBS(GYD_VCCR);

/*Macros*/
%LET BASE_GERENTES= gerentes_2020_03;

%LET RUTA_CONVENIOS = D:\MUESTRAS\SOBREGIROS\santi\Convenios - ciudades migrados2.xlsx; /*Ruta convenios de libranzas migrados*/

%LET RUTA_CUADROS = D:\MUESTRAS\SOBREGIROS\santi\Cuadros.xlsx; /*Ruta cuadros oficinas, agencias y montos*/

%LET IP =82.33.140.112;
/*Se renombra la base*/
data temp;
set END_MAR_20;
run;
/*Se validan los valores de ingresos*/
data temp_01;
 set temp;
  if ING_VARIABLE eq (.) then  ING_VARIABLE = 0;
  if ING_FIJO eq (.) then ING_FIJO = 0;	
  if DEDUCCIONES_NOM eq (.) then DEDUCCIONES_NOM =0;
  if porcentaje_ingresos eq (.) then porcentaje_ingresos = 0;
  if Total_resto_consumo eq (.) then Total_resto_consumo = 0;
  if CUPOS_ROTATIVO_&END eq (.) then CUPOS_ROTATIVO_&END = 0;
  if Saldo_consumo_&END eq (.) then Saldo_consumo_&END = 0;
  if Saldo_LIBRANZA_&END eq (.) then Saldo_LIBRANZA_&END = 0;
  if total_consumo eq (.) then total_consumo = 0;
  if Saldo_HIPOTECARIO_&END eq (.) then Saldo_HIPOTECARIO_&END = 0;
  if Saldo_COMERCIAL_&END eq (.) then Saldo_COMERCIAL_&END = 0;
  if CUPOS_TDC_&END eq (.) then CUPOS_TDC_&END = 0;
  if ingreso_total eq (.) then ingreso_total = 0;

  ingreso_total = ING_VARIABLE+ING_FIJO+DEDUCCIONES_NOM;
  porcentaje_ingresos = ING_VARIABLE/ingreso_total;
  Total_resto_consumo = CUPOS_ROTATIVO_&END+Saldo_consumo_&END;
  total_consumo = Saldo_LIBRANZA_&END+Total_resto_consumo;
  total_global_cliente = CUPOS_TDC_&END+total_consumo+Saldo_HIPOTECARIO_&END+Saldo_COMERCIAL_&END;
run;
/*Se traer el COD de la base categoria*/
proc sql; create table cruce as select
a.*,
b.COD
from temp_01 as a left join categoria as b
on SUCURSAL=COD;quit;
/*Se trae quien reporta de la base agencias*/
proc sql; create table cruce_2 as select
a.*,
b.Quien_reporta
from cruce as a left join agencia as b
on SUCURSAL= Cod_Agencia ;quit;
/*Se rellena los COD vaciosn con quien reporta*/
data temp_02;
 set cruce_2;
  if COD eq (.) then COD = Quien_reporta;
  rename COD = 'Sucursal final'n;
run;
/*Se elimina variable quien reporta*/
data temp_03;
 set temp_02;
  drop Quien_reporta;
run;
/*Se trae la categoria de la oficina*/
proc sql; create table cruce_final as select
a.*,
b.cat_ofi as categoria 
from temp_03 as a left join categoria as b
on 'Sucursal final'n= COD ;quit;
/*Se llena la categoria con el organismo decisor diferente a red*/
data temp_04;
 set cruce_final;
  if NUEVO_ORG_DEC in ('DIRECTOR DE ZONA','PREAPROBADO','TERRITORIAL') then categoria=NUEVO_ORG_DEC;
run;
/*Se traen los montos por categoria oficina*/
proc sql; create table cruce_montos as select
a.*,
b.*
from temp_04 as a left join montos as b 
on a.categoria = b.oficina;quit;
/*Se elimina la variable oficina*/
data montos_finales; 
 set cruce_montos;
  drop Oficina;
run;
/*F I L T R O S   F I N A L E S */
/*Para preaprobados el techo es "0"*/
data aux_2; 
 set montos_finales;
  if CATEGORIA eq 'PREAPROBADO' then uno_techo_directo = 0;
  run;
/* T e c h o   d i r e c t o */
/*Se marcan los posibles excesos de techo de acuerdo a los productos*/
data aux_2;
 set aux_2;
  if GRUPO_producto eq 'Libranza' and VALOR_DESEMBOLSO > 'Total Consumo'n then uno_techo_directo = 1;
  if GRUPO_producto in ('Libre Inv','Rotativo','Veh�culo') and VALOR_DESEMBOLSO > 'Resto Consumo'n then uno_techo_directo = 1;
  if GRUPO_producto eq 'Hipotecario' and VALOR_DESEMBOLSO > Hipotercario then uno_techo_directo = 1;
  if GRUPO_producto eq 'TDC' and VALOR_DESEMBOLSO > Tarjeta then uno_techo_directo = 1;
  if CATEGORIA eq 'PREAPROBADO' then uno_techo_directo = 0;
  if uno_techo_directo eq (.) then uno_techo_directo = 0;
run;
/* T e c h o   l i n e a */
/*Se marcan los posibles excesos de techo de acuerdo a los productos*/
data aux_2;
 set aux_2;
  if GRUPO_producto eq 'LIBRANZA' and total_consumo > 'Total Consumo'n then dos_techo_linea = 1;
  if GRUPO_producto in ('Libre Inv','Rotativo','Veh�culo') and Total_resto_consumo > 'Resto Consumo'n then dos_techo_linea = 1;
  if GRUPO_producto eq 'HIPOTECARIO' and Saldo_HIPOTECARIO_&END > Hipotercario then dos_techo_linea = 1;
  if GRUPO_producto eq 'TDC' and CUPOS_TDC_&END > Tarjeta then dos_techo_linea = 1;
  if total_global_cliente > 'Total Cliente'n then tres_techo_global = 1;
  if CATEGORIA eq 'PREAPROBADO' then dos_techo_linea = 0;
  if CATEGORIA eq 'PREAPROBADO' then tres_techo_global = 0;
  if uno_techo_directo eq (.) then dos_techo_linea = 0;
  if uno_techo_directo eq (.) then tres_techo_global = 0;
run;
/*Se marca los clientes autonomos*/
data autonomos;
 set aux_2;
  if SITUA_LABORAL = 3 then cuatro_autonomo = 1; 
  if SITUA_LABORAL = 4 and PROFESION not eq 'OE' then cuatro_autonomo = 1;
  if cuatro_autonomo eq (.) then cuatro_autonomo = 0;
run;
/*Se marca los incumplimientos en ingresos*/
data ingresos;
 set autonomos;
  if GRUPO_producto not eq 'HIPOTECARIO' and GRUPO_producto not eq 'LIBRANZA' and ingreso_total < 1500000 then cinco_ingresos = 1;
  if cinco_ingresos eq (.) then cinco_ingresos = 0;
  if LINEA_subproducto eq 'ROTATIVO' and ingreso_total < 1500000 then cinco_ingresos = 1;
run;
/*Se marca los incumplimientos en antiguedad*/
data antiguedad;
 set ingresos;
if SITUA_LABORAL eq 1 AND GRUPO_producto in ('Libre Inv','Rotativo','Veh�culo') AND ANTIGUO_LABO < 1 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 2 AND GRUPO_producto in ('Libre Inv','Rotativo','Veh�culo') AND ANTIGUO_LABO < 1 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 1 AND GRUPO_producto = 'LIBRANZA' AND ANTIGUO_LABO < 1 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 2 AND GRUPO_producto = 'LIBRANZA' AND ANTIGUO_LABO < 2 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 1 AND GRUPO_producto = 'tdc' AND ANTIGUO_LABO < 1 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 2 AND GRUPO_producto = 'tdc' AND ANTIGUO_LABO < 1 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 1 AND GRUPO_producto = 'HIPOTECARIO' AND ANTIGUO_LABO < 1 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 2 AND GRUPO_producto = 'HIPOTECARIO' AND ANTIGUO_LABO < 2 THEN seis_antiguedad = 1;
if SITUA_LABORAL eq 3 AND GRUPO_producto IN ('Libre Inv','Rotativo','Veh�culo','LIBRANZA','TDC','HIPOTECARIO') AND ANTIGUO_LABO < 2 THEN seis_antiguedad = 1;
if seis_antiguedad eq (.) then seis_antiguedad = 0;
run;
/*Se marcan los incumplimientos de buro*/
data buro_sin_rastro;
 set antiguedad;
  if DICTAMEN_BURO eq 'Aprobado' then siete_buro_sin_rastro = 0;
  if DICTAMEN_BURO eq 'Duda' then siete_buro_sin_rastro = 0;
  if DICTAMEN_BURO eq 'Sin Rastro' and GRUPO_producto not eq 'Libranza' then siete_buro_sin_rastro= 1;
  if siete_buro_sin_rastro eq (.) then siete_buro_sin_rastro= 1;
  if (FECHA_FORMALIZACION >= '18jan2016'd and GRUPO_producto in ('Rotativo','TDC')) THEN siete_buro_sin_rastro=0;
run;
/*Se marcan incumplimientos de montolimite*/
data monto_limite;
 set buro_sin_rastro;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq '1' and 75000000 <= VALOR_DESEMBOLSO <= 80000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq '2' and 70000000 <= VALOR_DESEMBOLSO <= 75000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq '3' and 65000000 <= VALOR_DESEMBOLSO <= 70000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq '4' and 60000000 <= VALOR_DESEMBOLSO <= 65000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq '5' and 55000000 <= VALOR_DESEMBOLSO <= 60000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq '6' and 45000000 <= VALOR_DESEMBOLSO <= 50000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq 'PERSONAL' and 145000000 <= VALOR_DESEMBOLSO <= 150000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq 'PREMIUM' and 195000000 <= VALOR_DESEMBOLSO <= 200000000 then once_monto_limite = 1;
  if LINEA_subproducto eq 'LIBRE INVERSION' and CATEGORIA eq 'Director' and 75000000 <= VALOR_DESEMBOLSO <= 80000000 then once_monto_limite = 1;
  if once_monto_limite eq (.) then once_monto_limite = 0;
run;
/*Se marcan incumplimientos de ingresos variables mayores*/
data ingresos_mayor;
 set monto_limite;
  if SITUA_LABORAL eq 1 and porcentaje_ingresos > 0.5 then trece_ingreso_mayor = 1;
  if SITUA_LABORAL eq 2 and porcentaje_ingresos > 0.5 then trece_ingreso_mayor = 1;
  if trece_ingreso_mayor = . then trece_ingreso_mayor = 0;
run;
/*Se marca los incumplimientos de profesi�n*/
data profesion;
 set ingresos_mayor;
  if PROFESION eq 'OB' then catorce_sin_profesion = 1;
  if PROFESION eq 'OD' then catorce_sin_profesion = 1;
  if PROFESION eq 'OF' then catorce_sin_profesion = 1;
  if PROFESION eq 'OG' then catorce_sin_profesion = 1;
  if catorce_sin_profesion eq (.) then catorce_sin_profesion = 0;
run;
/*Se marca incumplimientos de operaciones negadas*/
data op_negadas;
 set profesion;
  if negado_6m eq 'Si' then quince_op_negadas = 1;
  if quince_op_negadas = . then quince_op_negadas = 0;
run;
/*se marca incumplimientos de libre_inv*/
data lib_72_m;
 set op_negadas;
  if plazo > 72 and ingreso_total <6000000 and PUNTAJE_BURO < 770 and DICTAMEN_SCORE not eq 'APROBADO' and GRUPO_PRODUCTO = 'Libre Inv' then lib_72_m = 1;
   else lib_72_m = 0;
run;
/*Se trae la ciudad de las oficinas*/
proc sql; create table lib_72_m as select distinct
a.*,
b.CIUDAD
from lib_72_m as a left join pu_endeu.directorio_oficinas as b
on a.suc_obl_al=b.COD;quit; 
/*Se trae info de libranzas*/
proc sql; create table lib_72_m as select distinct
a.*,
b.Visto_Bueno, 
b.Nombre_empresa
from lib_72_m as a left join g_sg_min.CONSECUTIVOS_RANGOS as b
on a.cod_consec=b.cod_consec;quit; 
/*Se genera variable migraci�n*/
data lib_72_m;
 set lib_72_m;
  format migracion $char50.;
   migracion = cat(substr(cod_consec,1,3),CIUDAD);
run;
/*Se trae la plaza migrada*/
proc sql; create table lib_72_m as select distinct
a.*,
b.Plaza_migrada
from lib_72_m as a left join convenios_migrados as b
on a.migracion=b.Convenio_Ciudad;quit; 
/*Se marca incumplimientos de militares y ingresos en libranzas*/
data lib_72_m;
 set lib_72_m;
if GRUPO_PRODUCTO eq 'Libranza' and ingreso_total < 877803 and ori_final ne 'Preaprobado' then libranza_ingresos =1;
if GRUPO_PRODUCTO not in ('Libranza','Hipotecario','TDC') and profesion in ('EL','EM') THEN marca_militaries = 1;
run;
/*Se marca incumplimientos de no cliente*/  /******VALIDAR INFORMACI�N*******/
data no_cliente;
 set lib_72_m;
  if SI_NO_CLIENTE eq 'NO' then diez_no_cliente = 1;
run;
/*Se marca incumplimientos en buro*/  
data Buro;
 set no_cliente;
If GRUPO_PRODUCTO eq "Libre Inv" and SI_NO_NOMINA eq "SI" and PUNTAJE_BURO < 650 then Buro = 1;
If GRUPO_PRODUCTO eq "Libre Inv" and SI_NO_NOMINA eq "NO" and PUNTAJE_BURO < 770 then Buro = 1;
If GRUPO_PRODUCTO eq "Rotativo" and SI_NO_NOMINA eq "SI" and PUNTAJE_BURO < 650 then Buro = 1;
If GRUPO_PRODUCTO eq "Rotativo" and SI_NO_NOMINA eq "NO" and PUNTAJE_BURO < 770 then Buro = 1;
If GRUPO_PRODUCTO eq "Rotativo" and SITUA_LABORAL eq 3 and PUNTAJE_BURO < 750 then Buro = 1;
If GRUPO_PRODUCTO eq "Libranza" and PUNTAJE_BURO < 540 then Buro = 1;
If GRUPO_PRODUCTO eq "Hipotecario" and PUNTAJE_BURO < 650 then Buro = 1;
If GRUPO_PRODUCTO eq "Veh�culo" and PUNTAJE_BURO < 650 then Buro = 1;
If GRUPO_PRODUCTO eq "TDC" and SI_NO_NOMINA eq "SI" and PUNTAJE_BURO < 650 then Buro = 1;
If GRUPO_PRODUCTO eq "TDC" and SI_NO_NOMINA eq "NO" and PUNTAJE_BURO < 770 then Buro = 1;
If GRUPO_PRODUCTO eq "TDC" and SITUA_LABORAL eq 3 and PUNTAJE_BURO < 850 then Buro = 1;
if Buro eq (.) then Buro = 0; 
run;

/*Se marca en la base las alertas finales*/
data muestra_final;
 set BURO;
if BURO eq 1 then muestra_final ='Buro_malo';
if Visto_Bueno eq 'SIN VoBo' then muestra_final ='Libranza sin VoBo.';
if Plaza_migrada eq 'SI' THEN MUESTRA_FINAL ='Libranza Migrada';
if catorce_sin_profesion eq 1 then muestra_final ='catorce_sin_profesion';
if dieciseis_monto_li eq 1 then muestra_final ='dieciseis_monto_li';
if trece_ingreso_mayor eq 1 then muestra_final ='trece_ingreso_mayor';
if once_monto_limite eq 1 then muestra_final ='once_monto_limite';
if diez_no_cliente eq 1 then muestra_final ='diez_no_cliente';
/*if ocho_mas_3_scoring eq 1 then muestra_final ='ocho_mas_3_scoring';*/
if quince_op_negadas eq 1 then muestra_final ='quince_op_negadas';
if siete_buro_sin_rastro eq 1 then muestra_final ='siete_buro_sin_rastro';
if seis_antiguedad eq 1 then muestra_final ='seis_antiguedad';
if cinco_ingresos eq 1 then muestra_final ='cinco_ingresos';
if cuatro_autonomo eq 1 then muestra_final ='cuatro_autonomo';
if tres_techo_global eq 1 then muestra_final ='tres_techo_global';
if dos_techo_linea eq 1 then muestra_final ='dos_techo_linea';
if uno_techo_directo eq 1 then muestra_final ='uno_techo_directo';
if muestra_final eq ' ' then muestra_final = '0';
if 'Sucursal final'n eq (.) then delete;
run;
/*Se trae la info de la base de gerentes*/
proc sql; create table muestra_final as select
a.*,
b.*
from muestra_final as a left join g_sg_min.&BASE_GERENTES as b  
on a.'Sucursal final'n=b.COD;quit; 
/*Se crea base con las oris del mes*/
data oris;
 set pu_herme.ORI_CON_2020_07 pu_herme.ORI_TAR_2020_07 pu_herme.ORI_HIP_2020_07;
run;
/*Se elimina la variable estrategia_t1*/
data muestra_final;
 set muestra_final;
  drop estrategia_t1;
run;
/*Se trae la variable estrategia_t1*/
proc sql; create table muestra_final as select distinct
a.*,
b.estrategia_t1
from muestra_final as a left join oris as b
on (a.suc_obl_al=b.suc_obl_al and a.numero_op=b.numero_op);
quit;
/*Se ajusta el contenido de la variable y se crea la variable estrategia*/
data muestra_final;
 set muestra_final;  
if estrategia_t1 ='BANT1' then tem= 1;
if estrategia_t1 ='BANCARIZ' then tem= 1 ;
if estrategia_t1 ='LIBT1' then tem= 2;
if estrategia_t1 ='LIBRANZA' then tem= 2;
if estrategia_t1 ='VEHT1' then tem= 3;
if estrategia_t1 ='VEHICULO' then tem= 3;
if estrategia_t1 ='NBAT1' then tem= 4;
if estrategia_t1 ='NOBANCAR' then tem= 4;
if estrategia_t1 ='HPRCVT1' then tem= 5;
if estrategia_t1 ='HPRCV' then tem= 5;
if estrategia_t1 ='HPRPVT1' then tem= 6;
if estrategia_t1 ='HPRPV' then tem= 6;
if estrategia_t1 ='HPRNCT1' then tem= 7;
if estrategia_t1 ='HPRNC' then tem= 7;
if estrategia_t1 ='HIPSUT1' then tem= 8;
if estrategia_t1 ='HIPSUB' then tem= 8;
if estrategia_t1 ='RPRCVT1' then tem= 9;
if estrategia_t1 ='RPRPVT1' then tem= 10;
if estrategia_t1 ='RPRNCT1' then tem= 11;
if estrategia_t1 ='REDSUST' then tem= 12;
if estrategia_t1 ='REDSUST1' then tem= 12;
if estrategia_t1 ='REDSUNT' then tem= 13;
if estrategia_t1 ='REDSUNT1' then tem= 13;
if estrategia_t1 =' ' then tem= 14;
 
format estrategia $char20.;

if tem=1 then estrategia= 'BANCAR';
if tem=2 then estrategia = 'LIBZA';
if tem=3 then estrategia = 'VEHIC';
if tem=4 then estrategia = 'NO_BAN';
if tem=5 then estrategia = 'HIP_PCV';
if tem=6 then estrategia = 'HIP_PPV';
if tem=7 then estrategia = 'HIP_PNC';
if tem=8 then estrategia = 'HIP_SUB';
if tem=9 then estrategia = 'TDC_PCV';
if tem=10 then estrategia = 'TDC_PPV';
if tem=11 then estrategia = 'TDC_PNC';
if tem=12 then estrategia = 'TDC_SSC';
if tem=13 then estrategia = 'TDC_SNC';
if tem=14 then estrategia ='NN';
run;
/*Se elimina la variable*/
data muestra_final;
 set muestra_final;
  drop Cat_ofi COD tem;
run;
/*Frecuencia de alertas*/
proc freq data= muestra_final; table muestra_final;quit;
/*Se elimina variable y se crea TP_2*/
data muestra_final;
 set muestra_final;
  drop CLASE_TDC;
TP_2 =COMPRESS(BIN_TP,' ');
run;
/*Se trae la variable de base de bines*/
proc sql; create table muestra_final as select distinct
a.*,
b.CLASE_TDC
from muestra_final as a left join g_sg_min.BINES as b
on a.TP_2=b.TP;quit; 
/*Se asigna libreria*/
LIBNAME DATACIR BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/datacir" ;
/*Se trae nombre de modelo*/
proc sql; create table muestra_final_1 as select 
a.*,
b.Nombre_Modelo
from muestra_final as a left join datacir.tabla_modelos b on
    a.Estrategia_t1 = b.Estrategia_t1; 
quit;
data muestra_final;
 set muestra_final;
  drop bin_tp;
run;
/*Se asigna libreria*/
LIBNAME DATACIR BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/datacir" ;
/*Se tra informaci�n de la base clientes importantes*/
proc sql;
create table muestra_final as
select a.*,
b.NOMBRE_PEPS_PRPS,
b.NACIONALIDAD,
b.TITULO,
b.SITUACION,
b.ENTIDAD,
b.CARGO,
b.FEALTCLI,
b.FUENTE_FEC_DE_INI,
b.FTE_FEC_INICIO,
b.FEC_TERMIN_S1,
b.PAIS_INFLUEN_CARGO,
b.DEPTO_INFLU_CARGO,
b.CIUD_MUN_INFLU_CARGO
from muestra_final as a left join datacir.clientes_importantes as b
on a.identificacion = b.nit_sin;
quit; 
/*Se genera una base de la tabla de terceros con el nit ajustado*/
data terceros;
set pu_endeu.tabla_terceros;
nit1 = NIT_ORIG*1; 
run;
/*Se trae el nombre del cliente de la base originada anteriormente*/
proc sql;
create table muestra_final1 as
select a.*,
b.nombre
from Muestra_final a left join TERCEROS b on
 a.Tipo_documento = b.tp and a.Identificacion = b.nit1;
quit;
/*Se trae informaci�n de garantias*/
/*proc sql;
create table muestra_final1 as
select a.*,
b.OFICI_GTIA,
b.numero_gti,
b.garantia,
b.vr_gta,
b.vr_contabl
from muestra_final1 a left join pu_endeu.GT1219 b on
    a.suc_obl_al = b.suc_obl_al and
    a.numero_op = b.numero_op;
quit;
proc sort nodupkey data=muestra_final1;by suc_obl_al numero_op;run;
proc sql; create table muestra_final1 as
select a.*,
b.DETALLE_CODIGO_GARANTIA
from muestra_final1 a left join Pu_endeu.TABLA_GARANTIAS_SPAIN b on
    a.GARANTIA = b.GARANTIA ;
quit;
PROC SORT DATA=muestra_final1; BY FECHA_FORMALIZACION IDENTIFICACION;RUN;
/*Se debe importar la base historica para cruza con la base de infocliente*/
proc sql;
create table muestra_final1 as select 
a.*,
b.consulta_genero_alerta,
b.soi_salario_basico
from muestra_final1 a left join BASE_HISTORICA_MAR_20 b on
    a.IDENTIFICACION = b.identificacion_cliente;
quit;

proc sql;
create table muestra_final1_test as
select a.*,
b.NUMERO_OP as NUMERO_OP123
from muestra_final1 a left join g_sg_min.org_decisor_paq b on
    a.suc_obl_al = b.COD_OFICINA_GENERADA and
    a.numero_op = b.OPERACION_GENERADA;
quit;
*/
DATA G_SG_MIN.muestra_final1_test; */SET muestra_final1_test;*/RUN;*/

/*Variable mas de tres scoring no se volvio a correr*/
/*Variables de First, Second y Ever ya estan incluidas en la FM*/
/*Novedades documentales ya no se remite informe*/
/*Se incluye alertamiento por incumplimientos en buro 20/12/19 */