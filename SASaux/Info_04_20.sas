LIBNAME ARCHIVOS BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/archivos" ;
LIBNAME ARCHIVOS BASE "/SASDataRiesgos/FTP_sasus13/Bases_Sas/GE_PARAM/archivos" ;
LIBNAME ENDEUDAM BASE "/SASDataRiesgos/FTP_sasus7/ENDEUDAMIENTO" ;
OPTIONS MPRINT MLOGIC SYMBOLGEN;
%LET Dir = /SASData/FTP_sasus18;
%PUT _USER_;

/***	ASIGNAR AUTO LIBRERIAS - SAS SERVIDOR	***/
%MACRO LIBS(name);
	LIBNAME &name.  META LIBRARY=&name.  REPNAME= Foundation  METAOUT=DATA;
%MEND;

Data Hora_inicio;
	format 	Hora_inicio time.;
				Hora_inicio = time();
	put 		Hora_inicio = time.;
run;
%LIBS(G_SG_MIN);
%LIBS(PU_CLIEN);
%LIBS(PU_CMP);
%LIBS(PU_ENDEU);
%LIBS(PU_HERME);   
%LIBS(PU_SEGUI);
%LIBS(SERM_BAS);
%LIBS(GYD_VCCR);
/***********************PROGRAMA_INFOCLIOENTE*********************/
/*Imoprtar archivo de infocliente*/

/*PRIMERAS DEPURACIONES DEL REPORTE MENSUAL INFOCLIENTE*/

data REP_FINAL_INFO_APR_2020_test;
set REPORTE_FINAL_INFOCLIENTE_APR20;
if Respuesta_Final_Infocliente = 'TIME OUT  - CONSULTA NO VALIDA - INFORME A LA MESA DE AYUDA Y CONSULTE POSTERIORMENTE' then delete;
run;

proc sort data=REP_FINAL_INFO_APR_2020_test; 
by descending fecha_consulta
   descending hora_consulta; run;

proc sort nodupkey data=REP_FINAL_INFO_APR_2020_test;by identificacion_cliente;run;

data REP_FINAL_INFO_APR_2020_test;
set REP_FINAL_INFO_APR_2020_test;
keep id_consulta fecha_consulta hora_consulta codigo_funcionario nombre_oficina identificacion_cliente 
consulta_genero_alerta Respuesta_Final_Infocliente tipo_cliente soi_salario_basico;
run; 

/*UNE REPORTE MENSUAL INFOCLIENTE CON EL HISTORICO DE INFOCLIENTE*/

data BASE_HISTORICA_APR_20;
	 set BASE_HISTORICA_MAR_20 REP_FINAL_INFO_APR_2020_test;
RUN;	

data BASE_HISTORICA_APR_20;
set BASE_HISTORICA_APR_20;
if fecha_consulta >= '1JAN2020'd;
run;

proc sort data=BASE_HISTORICA_APR_20;  
by descending fecha_consulta;
run;

proc sort nodupkey data=BASE_HISTORICA_APR_20;by identificacion_cliente;run;

/*IMPORTA DATA DE BASE TOTAL_FORM DEL MES EN EJERCICIO OJO MODIFICAR MENSUALMENTE EL CAMPO DE FECHA*/

data op_mes;
set G_SG_MIN.TOTAL_FORM;
where '1APR2020'd <= FECHA_FORMALIZACION <= '30APR2020'd;
if ori_final = 'Riesgos' then delete;
if ori_final = 'Funcionarios' then delete;
if ori_final = 'Normalizaciones' then delete;
run;
PROC SQL; CREATE TABLE cruce_info_cli AS SELECT DISTINCT 
		  t1.TIPO_DOCUMENTO, 
          t1.IDENTIFICACION, 
          t1.SUC_OBL_AL, 
          t1.NUMERO_OP, 
          t1.FECHA_FORMALIZACION, 
          t1.VALOR_DESEMBOLSO, 
          t1.LINEA_SUBPRODUCTO, 
          t1.NOMBRE_SUBPRODUCTO,
		  t1.PROFESION,
		  t1.GRUPO_PRODUCTO, 
          t1.ORI_FINAL, 
          t1.ORG_DECISOR, 
          t1.PROFESION, 
          t1.SITUA_LABORAL, 
          t1.TP_TARJETAS,
		  t1.FECHA_EVAL_RES,
		  t1.ING_TOTAL,
          t2.IDENTIFICACION_CLIENTE, 
          t2.id_consulta, 
          t2.fecha_consulta, 
          t2.consulta_genero_alerta, 
          t2.codigo_funcionario, 
          t2.TIPO_CLIENTE, 
          t2.Respuesta_Final_Infocliente
      FROM op_mes t1 LEFT JOIN BASE_HISTORICA_APR_20 t2 
	  ON (t1.IDENTIFICACION = t2.IDENTIFICACION_CLIENTE);
QUIT;

/* DEFINE RESULTADO DEL CAMPO "respuesta_final" OJO TENER EN CUENTA EL CAMBIO DE LAS FECHA DE ACUERDO A MAS DE 6 O MENOS DE 6 MESES*/

data cruce_info_cli_test;
set cruce_info_cli;
format respuesta_final $200.;
valida_fechas = fecha_eval_res - fecha_consulta;
if GRUPO_PRODUCTO = 'Libranza' THEN respuesta_final = 'NO APLICA';
if org_decisor = 'VENTA CRUZADA' THEN respuesta_final = 'NO APLICA';
if ori_final = 'Preaprobado' THEN respuesta_final = 'NO APLICA';
if org_decisor = 'TDC D+0' THEN respuesta_final = 'NO APLICA';
IF profesion = 'EL' THEN respuesta_final = 'NO APLICA';
IF profesion = 'EM' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'T3' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'HW' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'LP' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'KW' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'KL' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'KT' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'KA' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'LA' THEN respuesta_final = 'NO APLICA';
IF TP_TARJETAS = 'LO' THEN respuesta_final = 'NO APLICA';
if respuesta_final = " " and fecha_consulta >= '01JAN20'd then respuesta_final = consulta_genero_alerta;
if respuesta_final = " " and valida_fechas <= 90 then respuesta_final = consulta_genero_alerta;
if respuesta_final = " " then respuesta_final = "NO CONSULTA";
if respuesta_final = "NO CONSULTA" and Respuesta_Final_Infocliente = 'VALIDACIoN  EXITOSA, CLIENTE NO PRESENTA NINGuNA ALERTA' and SITUA_LABORAL ne 3 then respuesta_final = "NO";
run;

proc sql ; 
select count(respuesta_final),ori_final, respuesta_final
from cruce_info_cli_test
group by ori_final, respuesta_final;
quit;

proc sql;
create table cruce_info_cli_test as
select a.*,
b.NOMBRE_PROFESION
from cruce_info_cli_test a left join archivos.tabla_profesiones b on
    a.PROFESION = b.PROFESION;
quit;

proc sql;
create table cruce_info_cli_test as
select a.*,
b.Nombre_situacion_laboral
from cruce_info_cli_test a left join archivos.situacion_laboral b on
    a.SITUA_LABORAL = b.SITUA_LABORAL;
quit;

proc sql;
create table cruce_info_cli_test as
select a.*,
b.sucursal_final
from cruce_info_cli_test a left join archivos.tabla_agencias b on
    a.SUC_OBL_AL = b.SUC_OBL_AL;
quit;

data cruce_info_cli_test;
set cruce_info_cli_test;
if sucursal_final = . then sucursal_final = suc_obl_al;
run;

proc sql;
create table cruce_info_cli_test as
select a.*,
b.oficina,
b.territorial,
b.zona
from cruce_info_cli_test a left join pu_endeu.tabla_sucursales b on
    a.sucursal_final = b.sucursal;
quit;
/*
PROC EXPORT DATA=cruce_info_cli_TEST
            OUTFILE= "D:\INFOCLIENTE\junio_2017\cruce_info_cli_junio_TEST1.xls" 
            DBMS=EXCELCS REPLACE;
            SERVER="82.33.140.112";
            PORT=9622;
            SHEET="data"; 
RUN;*/
