OPTIONS MPRINT MLOGIC SYMBOLGEN;
%LET Dir = /SASDataRiesgos/FTP_sasus18;
%PUT _USER_;

/***	ASIGNAR AUTO LIBRERIAS - SAS SERVIDOR	***/
%MACRO LIBS(name);
	LIBNAME &name.  META LIBRARY=&name.  REPNAME= Foundation  METAOUT=DATA;
%MEND;

%LIBS(G_SG_MIN);
%LIBS(PU_CLIEN);
%LIBS(PU_CMP);
%LIBS(PU_ENDEU);
%LIBS(PU_HERME);   
%LIBS(PU_SEGUI);
%LIBS(SERM_BAS);

%LET END =MAR_20; /*Mes y a�o de la asignaci�n*/

%LET FM= FM02_20200331; /*FM 02 del mes de la asignaci�n*/

%LET A= 2020; /*A�o de la asignaci�n*/

%LET M=03; /*Mes de la asignaci�n*/

/***Se extrae del FM 02 del mes y se dejan unicamente las variables se�aladas ***/

data &END;
SET SERM_BAS.&FM;
IF Tipo_Cartera IN (1,2,3);
	KEEP 	Identificacion 
			Centro_Altamira 
			Numero_OP 
			Subproducto 
			Tipo_Cartera 
			Producto 
			Fecha_Formalizacion 
			Valor_Desembolso 
			Dias_Vencidos_Mis 
			Meses_Vencidos_Mis 
			Capital_Activo;
RUN;
/***De la base extraida del mes se cruza la linea con la base de subproducto y se pega la linea y el grupo producto***/

proc sql; create table &END as select
a.*,
b.GRUPO_producto, 
b.LINEA_subproducto
from &END as a left join PU_ENDEU.BASE_SUBPRODUCTOS as b 
on a.subproducto = b.subproducto;
quit;
/***** ENDEUDAMIENTO*****/
/*** Se genera base unicamente con los nit y se eliminan duplicados***/
DATA NIT;
SET &END;
KEEP Identificacion;
RUN;

proc sort nodupkey data=NIT; by Identificacion;run;
/***********TARJETA**************/
/*Se genera una base para TDC, se cuantifica cantidad y valor por cliente*/
DATA TARJETA;
SET &END;
IF LINEA_subproducto IN ('TDC');
RUN;

PROC MEANS DATA=TARJETA VARDEF=DF SUM NOPRINT NWAY MISSING;
VAR Valor_Desembolso;
CLASS Identificacion;
OUTPUT OUT= pareto
SUM = CUPOS_TDC_&END; 
RUN;

DATA PARETO;
SET PARETO;
NUM_TDC_&END=_FREQ_;
RUN;

PROC SORT DATA=PARETO;BY Identificacion;RUN;
PROC SORT DATA=NIT;BY Identificacion;RUN;
/*Se cruza la base de TDC con la base de Nit*/
DATA NIT;
MERGE NIT (IN=A) PARETO (IN=B);
BY Identificacion ;
IF A=1;
RUN;

DATA NIT;
SET Nit;
DROP _TYPE_ _FREQ_;
RUN;
/**** CONSUMO ****/
/*Se genera una base para Consumo, se cuantifica cantidad y valor por cliente*/

DATA CONSUMO;
SET &END;
IF GRUPO_producto IN ('CONSUMO')AND LINEA_subproducto NOT IN ('LIBRANZA');
RUN;

PROC MEANS DATA=CONSUMO VARDEF=DF SUM NOPRINT NWAY MISSING;
VAR Capital_Activo;
CLASS Identificacion;
OUTPUT OUT= pareto
SUM = Saldo_consumo_&END;
RUN;

PROC SORT DATA=PARETO;BY Identificacion;RUN;
PROC SORT DATA=NIT;BY Identificacion;RUN;
/*Se cruza la base de Consumo con la base de Nit*/
DATA NIT;
MERGE NIT (IN=A) PARETO (IN=B);
BY Identificacion ;
IF A=1;
RUN;

DATA NIT;
SET NIT;
DROP _TYPE_ _FREQ_;
RUN;
/************ LIBRANZA ********************/
/*Se genera una base para Libranza, se cuantifica cantidad y valor por cliente*/

DATA LIBRANZA;
SET &END;
IF LINEA_subproducto IN ('LIBRANZA');
RUN;

PROC MEANS DATA=LIBRANZA VARDEF=DF SUM NOPRINT NWAY MISSING;
VAR Capital_Activo;
CLASS Identificacion;
OUTPUT OUT= pareto
SUM = Saldo_LIBRANZA_&END;
RUN;

PROC SORT DATA=PARETO;BY Identificacion;RUN;
PROC SORT DATA=NIT;BY Identificacion;RUN;
/*Se cruza la base de Libranza con la base de Nit*/
DATA NIT;
MERGE NIT (IN=A) PARETO (IN=B);
BY Identificacion ;
IF A=1;
RUN;

DATA NIT;
SET NIT;
DROP _TYPE_ _FREQ_;
RUN;
/************ ROTATIVO ********************/
/*Se genera una base para Rotativo, se cuantifica cantidad y valor por cliente*/

DATA ROTATIVO;
SET &END;
IF LINEA_subproducto IN ('ROTATIVO');
RUN;
PROC MEANS DATA=ROTATIVO VARDEF=DF SUM NOPRINT NWAY MISSING;
VAR Valor_Desembolso;
CLASS Identificacion;
OUTPUT OUT= pareto
SUM = CUPOS_ROTATIVO_&END; 
RUN;

PROC SORT DATA=PARETO;BY Identificacion;RUN;
PROC SORT DATA=NIT;BY Identificacion;RUN;
/*Se cruza la base de Rotativo con la base de Nit*/
DATA NIT;
MERGE NIT (IN=A) PARETO (IN=B);
BY Identificacion ;
IF A=1;
RUN;

DATA NIT;
SET NIT;
DROP _TYPE_ _FREQ_;
RUN;
/****** HIPOTECARIO *************/
/*Se genera una base para Hipotecario, se cuantifica cantidad y valor por cliente*/

DATA HIPOTECARIO;
SET &END;
IF GRUPO_producto  IN ('HIPOTECARIO');
RUN;

PROC MEANS DATA=HIPOTECARIO VARDEF=DF SUM NOPRINT NWAY MISSING;
VAR Capital_Activo;
CLASS Identificacion;
OUTPUT OUT= pareto
SUM = Saldo_HIPOTECARIO_&END;
RUN;

PROC SORT DATA=PARETO;BY Identificacion;RUN;
PROC SORT DATA=NIT;BY Identificacion;RUN;
/*Se cruza la base de hipotecario con la base de Nit*/

DATA NIT;
MERGE NIT (IN=A) PARETO (IN=B);
BY Identificacion ;
IF A=1;
RUN;

DATA NIT;
SET NIT;
DROP _TYPE_ _FREQ_;
RUN;

PROC SORT nodupkey DATA=NIT;BY Identificacion ;RUN;
/******Comercial******/
/*Se genera una base para Comercial, se cuantifica cantidad y valor por cliente*/

DATA COMERCIAL;
SET &END;
IF Tipo_Cartera  IN (2);
RUN;

PROC MEANS DATA=COMERCIAL VARDEF=DF SUM NOPRINT NWAY MISSING;
VAR Capital_Activo;
CLASS Identificacion;
OUTPUT OUT= pareto
SUM = Saldo_COMERCIAL_&END;
RUN;

PROC SORT DATA=PARETO;BY Identificacion;RUN;
PROC SORT DATA=NIT;BY Identificacion;RUN;
/*Se cruza la base Comercial con la base de Nit*/
DATA NIT;
MERGE NIT (IN=A) PARETO (IN=B);
BY Identificacion ;
IF A=1;
RUN;

DATA NIT;
SET NIT;
DROP _TYPE_ _FREQ_;
RUN;
PROC SORT nodupkey DATA=NIT;BY Identificacion ;RUN;

/*Se crea base de la total_form*/

%let mes=total_form;

data &mes;
set g_sg_min.total_form;
run;

PROC SORT DATA=&mes;BY IDENTIFICACION;RUN;
/*Se dejan unicamente los nit del mes de revisi�n*/
DATA &mes;
MERGE &mes (IN=A) nit (IN=B);
BY Identificacion;
IF A=1;
RUN;

DATA /*pu_endeu.*/END_&END;
SET &MES;
IF ANO_FORM=&A AND MES_FORM=&M;
RUN;
/*Se dejan solo los campos requeridos y se elimina la cartera comercial*/
data temp;
 set /*G_SG_MIN.*/END_&END;
  keep 
TIPO_DOCUMENTO
IDENTIFICACION
SUC_OBL_AL
NUMERO_OP
SUCURSAL
FECHA_FORMALIZACION
VALOR_DESEMBOLSO
SUBPRODUCTO
TIPO_CARTERA
COD_CONSEC
BIN_TP
DICTAMEN_BURO
DICTAMEN_SCORE
PUNTAJE_BURO
SITUA_LABORAL
SI_NO_NOMINA
ACTIVIDAD
ANTIGUO_LABO
ING_VARIABLE
ING_FIJO
DEDUCCIONES_NOM
PROFESION
SEXO
EDAD
COMPRA_CARTERA
TIPO_VIVIENDA
LINEA_SUBPRODUCTO
NOMBRE_SUBPRODUCTO
GRUPO_PRODUCTO
ORI_FINAL
CANAL_ORIGEN
NUEVO_ORG_DEC
ORG_DECISOR
COD_ORG_DECISOR
FECHA_EVAL_RES
DICTAMEN_SCORE
ING_TOTAL
PLAZO
SI_NO_CLIENTE
TERRITORIAL
TIPO_SEGMENTO
CATEGORIA_TDC
NOMBRE_TARJETA
ZONA
OFICINA
TP_TARJETAS
MARCA_MOMENTO
FIRST_PD
SECOND_PD
EVER_N
CUPOS_TDC_&END
NUM_TDC_&END
Saldo_consumo_&END
Saldo_LIBRANZA_&END
CUPOS_ROTATIVO_&END
Saldo_HIPOTECARIO_&END
Saldo_COMERCIAL_&END;
if TIPO_CARTERA eq 2 then delete;
if CANAL_ORIGEN in ('Director de zona','Preaprobado','Red','Territorial');
if GRUPO_PRODUCTO in ('Hipotecario','Libranza','Libre Inv','Rotativo','TDC','Veh�culo');
run;

/*****Codigo para incorporar Negados******/
/*Se trae de total colas las operaciones negadas de riesgos*/
DATA NEGA; 
SET G_SG_MIN.ORG_DECISOR_CONS_TDC;
IF DICTAMEN_ER EQ 'DENEGADO' AND NUEVO_ORG_DEC IN ('RIESGOS'); 
RUN;
/*Se crea base de la total con operaciones del mes, se borra los nit "0" y se dejan las variables especificadas*/
DATA FORM; 
SET G_SG_MIN.TOTAL_FORM;
IF NUEVO_ORG_DEC IN ('RED','DIRECTOR DE ZONA','TERRITORIAL','PREAPROBADO') AND ANO_FORM = 2020 AND MES_FORM EQ 03
AND GRUPO_PRODUCTO IN ('Hipotecario','Libranza','Libre Inv','Rotativo','TDC','Veh�culo');
IF IDENTIFICACION NOT IN (0,.);
KEEP SUC_OBL_AL NUMERO_OP IDENTIFICACION FECHA_FORMALIZACION ANO_FORM MES_FORM ORI_FINAL NUEVO_ORG_DEC;
RUN;
/*Se elimina preaprobados que vengan de riesgos*/
DATA FORM1;
SET FORM;
IF NUEVO_ORG_DEC EQ 'PREAPROBADO' AND ORI_FINAL eq 'RIESGOS' THEN DELETE;
RUN;
/*Se crea base donde se trae la fecha de decisi�n y se pega a la generada de la total*/
PROC SQL; CREATE TABLE CRUCE_VAL AS SELECT DISTINCT 
A.*, 
B.FECHA_EVAL_RES
FROM FORM1 AS A LEFT JOIN NEGA AS B
ON (A.IDENTIFICACION = B.IDENTIF_CLIENTE);
QUIT;
/*Se especifica el tiempo de negado y se da formato*/
DATA CRUCE_VAL_1;
SET CRUCE_VAL;
IF FECHA_EVAL_RES NE .;
IF NUEVO_ORG_DEC IN ('RED','DIRECTOR DE ZONA','TERRITORIAL') THEN FECHA_LIMITE = INTNX('month',FECHA_FORMALIZACION,-6,"same");
IF NUEVO_ORG_DEC EQ 'PREAPROBADO' THEN FECHA_LIMITE = INTNX('month',FECHA_FORMALIZACION,-3,"same");
FORMAT FECHA_LIMITE DATE9.;
RUN;
/*Se marcan las operaciones que tuvieron negados en el tiempo establecido*/
DATA CRUCE_VAL_2;
SET CRUCE_VAL_1;
FORMAT NEGADO_6M $2.;
IF FECHA_LIMITE <= FECHA_EVAL_RES <= FECHA_FORMALIZACION THEN NEGADO_6M = 'Si';
RUN;
/*Solo se deja las operaciones negadas*/
DATA CRUCE_VAL_3;
SET CRUCE_VAL_2;
IF NEGADO_6M = 'Si';
RENAME FECHA_EVAL_RES = NEGADO_FECHA;
RUN;
/*** Quita duplicados y conserva la �ltima fecha en que se neg� en riesgos ***/
PROC SORT DATA=CRUCE_VAL_3; BY DESCENDING NEGADO_FECHA ; QUIT;
PROC SORT DATA=CRUCE_VAL_3 NODUPKEY; BY SUC_OBL_AL NUMERO_OP; QUIT;
/*Se cruzan y marcan las operaciones negadas con la primera base generada de la total con las operaciones del mes*/
PROC SQL; CREATE TABLE PRUEBA AS SELECT DISTINCT 
A.*, 
B.NEGADO_6M AS MAR_NEG, 
B.NEGADO_FECHA AS FEC_NEG
FROM temp AS A LEFT JOIN CRUCE_VAL_3 AS B
ON (A.SUC_OBL_AL = B.SUC_OBL_AL AND A.NUMERO_OP = B.NUMERO_OP);
QUIT;

DATA PRUEBA1;
SET PRUEBA;
IF MAR_NEG NE '' THEN NEGADO_6M = MAR_NEG;
IF FEC_NEG NE . THEN NEGADO_FECHA = FEC_NEG;
RUN;

DATA END_&END;
SET PRUEBA1;
DROP MAR_NEG FEC_NEG;
RUN;
