from extractSentence import isComment
import re

# Recibir la sentencia
# Extraer el tipo de comando
# Decidir que tipo de conversión según el tipo de comando
# Envio al tipo de conversión
# Recibo la conversión
# Retorno tipo de comando y conversión


def convertSentenceComment(type,sentence):
    sentence = sentence.strip()  # clean space
    sentenceConvert = sentence
    if type == '*':
        sentenceConvert = "// " + sentence[1:-1]
    return sentenceConvert


def getTypeSentence(sentence):

    sentenceConvert = ""

    cmd = sentence.split()
    firstWord = (cmd[0]).lower()
    flagComment = isComment(firstWord)

    if flagComment != '':  # Is comment
        typeCommand = "comment"
        sentenceConvert = convertSentenceComment(flagComment, sentence)
    else:
        typeCommand = firstWord

    #print(sentenceConvert)
    return [typeCommand, sentenceConvert]


def convertSentence(sentence):
    return 1

#convertSentence("* Hola comentarios ;")
#convertSentence("/* Hola comentarios */")